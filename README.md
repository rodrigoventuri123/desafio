# Desafio texo it!

## Instalação

Para rodar o projeto basta ter instaldo em sua maquina o java 13 e maven

## Como rodar o projeto

```bash
mvn clean install
mvn spring-boot:run
```

## Como rodar os teste

```bash
mvn test
```

## API

```bash
http://localhost:8080/api/interval
```
