package com.texoit.demo.resource;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MovieResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getMovieTest() throws Exception {
        mockMvc.perform(get("/api/interval")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.max", notNullValue()))
                .andExpect(jsonPath("$.min", notNullValue()));
    }

}
