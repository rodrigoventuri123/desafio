package com.texoit.demo.exception;

public class ReadFileException extends RuntimeException {

    public ReadFileException(String msg){
        super(msg);
    }

}
