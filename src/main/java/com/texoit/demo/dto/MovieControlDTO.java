package com.texoit.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MovieControlDTO {
    private String producer;
    private Integer interval;
    private Boolean hasConsecutive;
    private Integer yearMin;
    private Integer yearMax;
    private Integer yearMinCons;
}
