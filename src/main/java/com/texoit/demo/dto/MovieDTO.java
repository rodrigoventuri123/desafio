package com.texoit.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MovieDTO {

    private List<MovieDetail> max;
    private List<MovieDetail> min;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    @Builder
    public static class MovieDetail {
        private String producer;
        private Integer interval;
        private Integer previousWin;
        private Integer followingWin;
    }
}
