package com.texoit.demo.resource;

import com.texoit.demo.dto.MovieDTO;
import com.texoit.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/interval")
public class MovieResource {

    @Autowired
    private MovieService movieService;

    @GetMapping
    public MovieDTO getInterval() {
        return movieService.getAllMovieInterval();
    }

}
