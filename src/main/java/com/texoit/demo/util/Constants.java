package com.texoit.demo.util;

public final class Constants {
    public static final String CSV_NAME = "/movielist.csv";
    public static final String CSV_DELIMITER = ";";
    public static final String CSV_FIELD_WINNER_VALUE_DEFAULT = "yes";
}
