package com.texoit.demo.service;

import com.texoit.demo.domain.Movie;
import com.texoit.demo.exception.ReadFileException;
import com.texoit.demo.repository.MovieRepository;
import com.texoit.demo.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ReadFileService {

    @Autowired
    private MovieRepository movieRepository;

    @PostConstruct
    public void saveMovie() {
        Pattern pattern = Pattern.compile(Constants.CSV_DELIMITER);

        try (var br = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream(Constants.CSV_NAME)))) {
            List<Movie> collect = br.lines().skip(1).map(line -> {
                String[] x = pattern.split(line);
                Movie.MovieBuilder movieDTOBuilder = Movie.builder()
                        .year(Integer.parseInt(x[0]))
                        .title(x[1])
                        .studios(x[2])
                        .producers(x[3]);
                if (x.length == 5) {
                    movieDTOBuilder.winner(x[4]);
                }
                return movieDTOBuilder.build();
            }).collect(Collectors.toList());
            collect.forEach(movie -> {
                movieRepository.save(movie);
            });
        } catch (FileNotFoundException e) {
            throw new ReadFileException("Arquivo não encontrado");
        } catch (IOException e) {
            throw new ReadFileException("Erro na leitura do arquivo");
        }
    }

}
