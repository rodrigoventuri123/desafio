package com.texoit.demo.service;

import com.texoit.demo.dto.MovieControlDTO;
import com.texoit.demo.dto.MovieDTO;
import com.texoit.demo.domain.Movie;
import com.texoit.demo.repository.MovieRepository;
import com.texoit.demo.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public List<Movie> findAllByWinner() {
        return movieRepository.findAllByWinner(Constants.CSV_FIELD_WINNER_VALUE_DEFAULT);
    }

    private Map<String, List<Movie>> movieGroupingByProducers(List<Movie> movies) {
        return movies.stream()
                .collect(Collectors.groupingBy(Movie::getProducers));
    }

    private List<Movie> sortByYear(List<Movie> movies) {
        return movies.stream().sorted(Comparator.comparing(Movie::getYear)).collect(Collectors.toList());
    }

    private void setHasConsecutiveAndSetYearMinCons(MovieControlDTO dto, List<Movie> moviesSortYear) {
        dto.setInterval(0);
        if(moviesSortYear.size() == 1) {
            dto.setInterval(0);
            dto.setHasConsecutive(Boolean.FALSE);
            dto.setYearMinCons(moviesSortYear.get(0).getYear());
        } else {
            moviesSortYear.stream().forEach(movie -> {
                Integer intervaloAtual = dto.getInterval();
                Integer year = movie.getYear();
                int i = year - intervaloAtual;
                if (i <= 1 && !Boolean.TRUE.equals(dto.getHasConsecutive())) {
                    dto.setHasConsecutive(Boolean.TRUE);
                    dto.setYearMinCons(intervaloAtual);
                }
                dto.setInterval(i);
            });
        }


    }

    private Integer calcInterval(Integer min, Integer max) {
        return max - min;
    }

    private void setMinAndMaxYear(MovieControlDTO dto, List<Movie> moviesSortYear) {
        Integer min = moviesSortYear.get(0).getYear();
        Integer max = moviesSortYear.get(moviesSortYear.size() - 1).getYear();
        dto.setYearMax(max);
        dto.setYearMin(min);
        dto.setInterval(this.calcInterval(min, max));
    }

    private List<MovieControlDTO> createDtoMovie(Map<String, List<Movie>> moviesGrouping) {
        List<MovieControlDTO> movieSQLDTOS = new ArrayList<>();
        moviesGrouping.forEach((producer, movies) -> {
            MovieControlDTO dto = new MovieControlDTO();
            dto.setProducer(producer);
            List<Movie> moviesSortYear = this.sortByYear(movies);
            this.setHasConsecutiveAndSetYearMinCons(dto, moviesSortYear);
            this.setMinAndMaxYear(dto, moviesSortYear);
            movieSQLDTOS.add(dto);
        });
        return movieSQLDTOS;
    }

    private MovieControlDTO filterMaxHasConsecutive(List<MovieControlDTO> movieControlDTOS) {
        Optional<MovieControlDTO> max = movieControlDTOS.stream().filter(movieSQLDTO -> Boolean.TRUE.equals(movieSQLDTO.getHasConsecutive()))
                .sorted(Comparator.comparing(MovieControlDTO::getInterval).reversed())
                .max(Comparator.comparing(MovieControlDTO::getYearMinCons));
        if (max.isEmpty()) {
            return new MovieControlDTO();
        }
        MovieControlDTO movieControlDTO = max.get();
        return movieControlDTO;
    }

    private MovieControlDTO filterMinHasConsecutive(List<MovieControlDTO> movieControlDTOS) {
        Optional<MovieControlDTO> min = movieControlDTOS.stream().filter(movieSQLDTO -> Boolean.TRUE.equals(movieSQLDTO.getHasConsecutive()))
                .sorted(Comparator.comparing(MovieControlDTO::getInterval).reversed())
                .min(Comparator.comparing(MovieControlDTO::getYearMinCons));
        if (min.isEmpty()) {
            return new MovieControlDTO();
        }
        MovieControlDTO movieControlDTO = min.get();
        return movieControlDTO;
    }

    private MovieControlDTO filterMaxInterval(List<MovieControlDTO> movieControlDTOS) {
        Optional<MovieControlDTO> min = movieControlDTOS.stream().filter(controlDTO -> controlDTO.getYearMinCons() != null).sorted(Comparator.comparing(MovieControlDTO::getInterval).reversed()).min(Comparator.comparing(MovieControlDTO::getYearMinCons));
        if (min.isEmpty()) {
            return new MovieControlDTO();
        }
        return min.get();
    }

    private MovieControlDTO filterMinInterval(List<MovieControlDTO> movieControlDTOS) {
        Optional<MovieControlDTO> min = movieControlDTOS.stream().filter(controlDTO -> controlDTO.getYearMinCons() != null).sorted(Comparator.comparing(MovieControlDTO::getInterval).reversed()).max(Comparator.comparing(MovieControlDTO::getYearMinCons));
        if (min.isEmpty()) {
            return new MovieControlDTO();
        }
        return min.get();
    }

    public MovieDTO getAllMovieInterval() {
        MovieDTO dto = new MovieDTO();
        List<Movie> moviesWinner = this.findAllByWinner();
        Map<String, List<Movie>> moviesGrouping = this.movieGroupingByProducers(moviesWinner);
        List<MovieControlDTO> movieControlDTOS = this.createDtoMovie(moviesGrouping);
        dto.setMax(new ArrayList<>());
        dto.setMin(new ArrayList<>());
        dto.getMax().add(this.convertMovieControlFromDetail(this.filterMaxHasConsecutive(movieControlDTOS)));
        dto.getMax().add(this.convertMovieControlFromDetail(this.filterMaxInterval(movieControlDTOS)));
        dto.getMin().add(this.convertMovieControlFromDetail(this.filterMinHasConsecutive(movieControlDTOS)));
        dto.getMin().add(this.convertMovieControlFromDetail(this.filterMinInterval(movieControlDTOS)));
        return dto;
    }

    private MovieDTO.MovieDetail convertMovieControlFromDetail(MovieControlDTO control) {
        return MovieDTO.MovieDetail.builder()
                .producer(control.getProducer())
                .interval(control.getInterval())
                .followingWin(control.getYearMax())
                .previousWin(control.getYearMin())
                .build();
    }

}
